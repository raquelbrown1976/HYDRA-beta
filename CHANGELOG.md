# Table of Contents
- [31 January 2021](#31-january-2021)
- [24 January 2021](#24-january-2021)
- [17 January 2021](#17-january-2021)
- [10 January 2021](#10-january-2021)
- [3 January 2021](#3-january-2021)
- [31 December 2020](#31-december-2020)
- [29 December 2020](#29-december-2020)
- [28 December 2020](#28-december-2020)
- [27 December 2020](#27-december-2020)
- [25 December 2020](#25-december-2020)
- [24 December 2020](#24-december-2020)
- [20 December 2020](#20-december-2020)
- [18 December 2020](#18-december-2020)
- [17 December 2020](#17-december-2020)
- [13 December 2020](#13-december-2020)
- [6 December 2020](#6-december-2020)
- [22 November 2020](#22-november-2020)
- [15 November 2020](#15-november-2020)
- [8 November 2020](#8-november-2020)
- [4 September 2020](#4-september-2020)
- [30 August 2020](#30-august-2020)
- [20 August 2020](#20-august-2020)
- [19 August 2020](#19-august-2020)
- [17 August 2020](#17-august-2020)
- [15 August 2020](#15-august-2020)
- [13 August 2020](#13-august-2020)
- [12 August 2020](#12-august-2020)

# 31 January 2021

## General
- Tiered upgrades have been removed

## AI
- Build selection logic has been optimized
- The following Protoss builds have been added:
	- General 4: Lunatic Legion (heavy Legionnaire and Aurora opening)
	- Templar 5: Martyr (Grand Library-focused opening)
	- Skylord 5: Sorrow Fleet (capital ship mix)
- The following Terran build has been added:
	- General 6: Observant (early Vessel mech opening)
- The following Zerg build has been added:
	- General 4: Zoryus Flood

## Bugfixes
- AI personality and build selection is no longer ignored in the early game
- AI build selection now correctly accounts for all possible builds when randomizing
- Terran AI now correctly use Heracles and correctly add Captaincy units to attack waves
- Munitions Bay build string has been corrected

## Terran
- Armory, Future Station:
	- Removed

## Zerg
- Evolution Chamber, Mutation Pit:
	- Removed
- Zoryus Shroud
	- No longer requires anything
	- Button moved to basic structures

## Protoss
- Envoy, Witness:
	- Now assembled at Forge
- Forge:
	- No longer researches upgrades
	- HP and shields now 500/500, from 550/550
	- Now costs 150 minerals and 50 gas, from 200/100
	- Sight range now 9, from 10
- Grand Library:
	- Collision box has been resized to allow better pathing around the structure

# 24 January 2021

## Editor
- Courtesy of DarkenedFantasies,
	- North-facing ashworld dirt ramps have been added
	- North-facing twilight dirt and basilica ramps have been added
- ASHWORLD:
	- Basilica and crushed rock have been ported from twilight
	- Lava-blended dirt cliffs have been added
- TWILIGHT:
	- A brush palette file has been added

## Bugfixes
- Arbiters no longer cloak resources
- AI now correctly respond to being attacked by air units
- Quarry damage overlay and construction animation have been corrected

## Terran
- Madcap:
	- Time cost now 15, from 25
- Heracles:
	- Time cost now 20, from 22
- Apostle:
	- Time cost now 25, from 30
- Salamander:
	- Splash type now ally-safe
- Penumbra:
	- Time cost now 50, from 60

## Protoss
- Atreus:
	- Weapon splash radius now 12/24/48, from 0/0/0
	- Stalwart Stance removed
- Barghest:
	- Gas cost now 75, from 50

## Zerg
- Iroleth:
	- HP now 200, from 250
- Othstoleth:
	- HP now 300, from 400
- Kalkalisk:
	- HP now 100, from 120
- Almaksalisk:
	- HP now 120, from 160

# 17 January 2021

## General
- Ground units now gain height advantage when targeting air units on lower height levels
- Several heavy air units have had their movement speeds reduced
- Sire now has a wireframe, courtesy of jun3hong!
- Sire has been downscaled to better fit the size profile of a Hatchery morph
- First-draft unit responses have been added for Madcaps and Ecclesiasts
- New firing sounds have been added for Ecclesiasts

## AI
- Expansion timings have been further optimized
- Extra expansions are further prioritized when floating excess money
- Static defense kicks in earlier and more frequently
- Production requests have been optimized

## Bugfixes
- Melee units no longer benefit from cliff/creep advantage
- Salamander Incendiary Payloads now properly benefit from attack upgrades
- Protoss and Zerg AI transport usage has been reinstated
- Zerg AI expansions are no longer disproportionately delayed based on Hatchery count
- Protoss AI no longer fail to add Argosies from scaling production functions
- Gorgrokors once again use their birth animation
- Blinded units now have an appropriate status displayed on their armor tooltip

## Terran
- Firebat, Cyprian:
	- No longer require attached Gallery
- Medic:
	- Now requires attached Medbay
	- Time cost now 15, from 18.75
- Shaman:
	- Adrenaline Packs passive removed
- Madcap:
	- Now trained at Captaincy
	- Mineral cost now 75, from 100
	- Time cost now 22, from 25
- Apostle:
	- Now trained at Captaincy
	- HP now 70, from 65
	- Gas cost now 150, from 175
	- Time cost now 30, from 36
- Cyclops:
	- HP now 80, from 90
	- Sight range now 7, from 8
	- Mineral cost now 75, from 100
	- Time cost now 20, from 28
- Gorgon:
	- No longer requires attached Munitions Bay
	- HP now 120, from 150
	- Weapon damage now 12, from 15
- Salamander:
	- Now trained at Starport; requires attached Munitions Bay
	- Now attacks both ground and air units
- Pegasus:
	- Removed, pending design revisit
- Centaur:
	- Now trained at Nanite Assembly
	- HP now 300, from 325
	- Time cost now 45, from 50
- Battlecruiser:
	- Damage now 10+1, from 25+3
	- Attack cooldown now 0.625, from 1.25
	- Attack range now 6, from 7
	- New passive: Monoceros Targeters:
		- Battlecruisers can attack up to 5 targets simultaneously
- Phobos:
	- No longer requires attached Particle Accelerator
- Comsat Station:
	- No longer requires Armory
- Armory:
	- HP now 800, from 750
	- Time cost now 32.5, from 45
- Medbay:
	- No longer requires anything
- Science Facility:
	- Now costs 500/500/60, from 600/600/70
- Gallery, Physics Lab, Particle Accelerator:
	- Removed
- New: Heracles:
	- Frontline mechanized infantry; trained from Captaincy
- New: Pazuzu:
	- Anti-mech hovertank; built from Iron Foundry
- New: Captaincy:
	- Advanced infantry production; built from SCV; requires Engineering Bay

## Zerg
- Evolution Chamber:
	- Mineral cost now 100, from 125
- Vorval Pond:
	- Mineral cost now 125, from 100
	- Gas cost now 0, from 50
- Quazil Quay:
	- Mineral cost now 100, from 150
	- Gas cost now 50, from 0
	- Time cost now 25, from 31.25
- Gorgrok Apiary:
	- Time cost now 31.25, from 42.5
- Queen's Nest:
	- Time cost now 31.25, from 37.5

## Protoss
- Envoy:
	- Formerly known as Shuttle
	- Speed now 72, from 48 (now matches Corsair/Mutalisk/Wraith/etc)
	- Acceleration now 27, from 22 (roughly 125% of the old value)
- Cabalist:
	- Formerly known as Dark Templar
	- Now trained at Gateway
	- Now requires Citadel of Adun
- Templar:
	- Formerly known as High Templar
- Amaranth:
	- Now trained at Grand Library
	- No longer requires anything
	- HP now 100, from 120
	- Time cost now 28, from 32.5
- Atreus:
	- Now trained at Grand Library
	- Shields now 120, from 140
	- Time cost now 28, from 32.5
- Mind Tyrant:
	- Formerly known as Dark Archon
- Arbiter:
	- Shroud of Adun:
		- Now cloaks own and allied buildings
		- Now cloaks other Arbiters (does not cloak self)
		- Now prevents permanently cloaked units (Cabalists, Barghests) from revealing on attack
- Grand Library:
	- Mineral cost now 200, from 250
	- Build space now 160x128, from 192x160
- Archon Archives:
	- Formerly known as Templar Archives

# 10 January 2021

## General
- Particle Accelerator has received a proper wireframe, courtesy of jun3hong!

## Editor
- ASH: New blends for lava -> dirtcliff and lava -> magmacliff have been added

## AI
- Wait times between expansion attempts have been reduced
- Military training commands have been optimized in minor ways
- Zerg AI are now slightly better at reaching tier 2 and 3 tech, and at balancing their worker-to-military ratios

## Bugfixes
- Several common crashes have been resolved, though this stability fix may introduce strange bugs in the mid-to-late-game when AI are involved; more complete fixes are in the works
- Particle Accelerators no longer require the Quantum Institute
- AI-controlled Ghosts no longer spam Lobotomy Mines
- Gorgon attacks now correctly refresh Observance, Hallucination, and Plague
- Quazilisks can now attack from within Bunkers
- Nanite Assemblies no longer have a button to train Penumbra
- Requirement strings for Grand Library, Robotics Authority, and Argosy have been corrected
- JUNGLE: Higher temple tiles have been color-corrected to blend with higher dirt

## Maps
- Two installments of Keyan's latest series, Just Another HYDRA Map, are now available

## Zerg
- Mutalisk:
	- Gas cost now 75, from 100
- Gorgrokor:
	- HP now 200, from 190
	- Armor now 1, from 2
- Devourer:
	- Gas cost now 25, from 50
- Zoryusthaleth:
	- HP now 180, from 120
	- Gas cost now 50, from 75
	- Collision box now 32x29, from 37x33
- Defiler:
	- Hp now 100, from 80
- Larval Colony:
	- HP now 300, from 350
	- Now requires nothing, from Irol Iris
- Devourer Haunt:
	- Mineral cost now 100, from 200
- Zoryus Shroud:
	- Mineral cost now 150, from 75
	- Gas cost now 75, from 125

## Protoss
- Simulacrum:
	- Now moves roughly 10% slower
	- Collision box now 22x20, from 26x24
	- Autovitality clones now inherit shields and debuffs from their parent
- Barghest:
	- Now requires nothing
- Clarion:
	- Now requires Strident Stratum

# 3 January 2021

## General
- Christmas graphics have been retired to ring in the new year
- The Medbay's graphics and command icon have been updated, courtesy of DarkenedFantasies!
- Command history has been added to the debug console, courtesy of Milestone!

## Bugfixes
- Quazilisks no longer play their idle animations while burrowed

## Zerg
- Creep:
	- For all Zerg units targeting ground units that are not on Creep, now provides a range advantage equivalent to that given by height advantage
	- This creep range advantage can stack with height range advantage
- Quazilisk:
	- Weapon range now 4, from 3
- Bactalisk:
	- Weapon range now 7, from 6
	- Corrosive Dispersion (range on creep) removed
- Creep, Sunken, Spore, Larval Colony:
	- Transport space cost now 8, from 6

# 31 December 2020

## General
**Happy new year from the No-Frauds Club!**

This patch features large-scale balancing with the intent to add more value to base infrastructure and tech without sacrificing the value of individual units. To accomplish this, I have reduced the time costs of most units at tier 2 and beyond, as well as several tier 1 units and all supply providers. While this will result in an increase in APM directed towards maintaining a supply bank, especially in the mid-game and if you do not opt into the supply-increasing mechanics for each race, I hope that this will encourage the use of later-tier units in a more fair manner. I also believe that this will better reward maintaining higher-tier production, while incentivizing attacks against enemy bases that hold such coveted infrastructure.

There will likely be outliers that need to be reigned in or buffed to maintain a healthy level of relevance, so your patience is appreciated while we conduct this experiment.

## AI
- Terran AI will now more frequently construct Nanite Assemblies when floating extra resources
- Zerg AI upgrade timings have been adjusted

## Bugfixes
- AI definition mismatches will now print messages onscreen instead of panicking
- Incorrect overlays will now print messages onscreen instead of crashing
- Hallucinations now inherit AI properly
- Various Protoss rubble underlays have been corrected

## Terran
- Apostle, Azazel:
	- Time cost now 36, from 40
- Shaman, Ghost, Goliath:
	- Time cost now 22, from 25
- Savant, Siege Tank, Dropship, Valkyrie:
	- Time cost now 28, from 31.25
- Southpaw:
	- Time cost now 20, from 25
- Cyclops:
	- Time cost now 28, from 30
- Madrigal:
	- Time cost now 28, from 32.5
- Penumbra:
	- Now trained at Iron Foundry and no longer requires attached Particle Accelerator
	- Now costs 450/350/60/8, from 700/500/80/10
	- HP now 500, from 750
	- Armor now 2, from 3
	- Weapon range now 10, from 12
- Wendigo:
	- Now costs 125/0/20, from 100/25/25
	- Armor now 1, from 0
	- Automedicus now heals for 25% of damage dealt, from 50%
- Wraith:
	- Time cost now 30, from 32.5
- Gorgon:
	- Time cost now 30, from 36
- Wyvern:
	- Time cost now 36, from 45
- Science Vessel:
	- Time cost now 40, from 50
- Centaur, Pegasus:
	- Time cost now 50, from 60
- Salamander:
	- Time cost now 28, from 30
- Battlecruiser:
	- Time cost now 60, from 70
- Phobos:
	- Now costs 500/350/65/10, from 800/600/80/12
	- HP now 800, from 1000
	- Armor now 3, from 4
	- Laser and missile attack cooldowns now 1.833, from 1.541
- Supply Depot:
	- Time cost now 22, from 25
- Quantum Institute:
	- This structure has been disabled until more tier 4 tech is implemented.
- New: Iron Foundry:
	- Advanced vehicle production; built from SCV; requires Science Facility

## Zerg:
- Overlord:
	- Time cost now 22, from 25
- Vorvaling:
	- Time cost now 10, from 12
- Mutalisk, Zoryusthaleth, Keskathalor:
	- Time cost now 22, from 25
- Iroleth, Bactalisk:
	- Time cost now 15, from 18
- Nathrokor:
	- Time cost now 12, from 12.5
- Lurker:
	- Time cost now 18.75, from 25
- Quazilisk:
	- Time cost now 12, from 15
- Kalkalisk:
	- Time cost now 15, from 18.75
- Gorgrokor:
	- Time cost now 15, from 20
- Othstoleth:
	- Time cost now 20, from 25
- Almaksalisk, Defiler, Guardian:
	- Time cost now 18, from 20
- Alkajelisk:
	- Time cost now 25, from 30
- Larval Colony:
	- Mineral cost now 25, from 75
	- Time cost now 12.5, from 20

## Protoss
- Shuttle:
	- Time cost now 28, from 30
- Witness:
	- Time cost now 22, from 25
- Dragoon, Idol:
	- Time cost now 30, from 31.25
- Ecclesiast:
	- Time cost now 28, from 25
- Legionnaire:
	- Shields now 50, from 60
- Hierophant:
	- Time cost now 30, from 35
- Amaranth, Atreus, Reaver:
	- Time cost now 32.5, from 37.5
- Dark Templar:
	- Time cost now 25, from 30
- Pariah:
	- Time cost now 45, from 60
- Golem:
	- Time cost now 30, from 37.5
- Clarion:
	- Gas cost now 125, from 100
	- Time cost now 28, from 30
- Architect:
	- HP now 200, from 250
	- Shields now 150, from 100
- Barghest:
	- Time cost now 22, from 25
- Panoptus:
	- Time cost now 35, from 37.5
- Exemplar, Gladius:
	- Time cost now 36, from 40
- Magister:
	- Time cost now 36, from 45
- Arbiter:
	- Time cost now 70, from 90
- Empyrean:
	- Ore cost now 325, from 350
	- Time cost now 50, from 60
- Carrier:
	- Ore cost now 425, from 400
	- Time cost now 60, from 87.5
- Star Sovereign:
	- HP now 350, from 300
	- Shields now 400, from 500
	- Time cost now 100, from 120
	- Supply cost now 10, from 8
- Pylon:
	- Time cost now 16, from 18.75

# 29 December 2020

## General
- Better anti-aliasing has been added for our Christmas present fields, courtesy of DarkenedFantasies!
- A Larval Colony command icon has been added, courtesy of DarkenedFantasies!
- Wireframes for our Christmas present fields have been added, courtesy of jun3hong!

## AI
- All AI will now save resources for an expansion when all bases are mined out
- Protoss build orders have been overhauled
- Zerg build order timings have been adjusted to provide more time for worker-to-army ratio balancing
- AI support for Larval Colonies and Ion Cannons has been added
- A new Protoss Ranger build order, Simulant Swarm, has been added (uses 5 Dark Swarm deaths)
- AI military training has been improved for all races

## Bugfixes
- AI-related crashes have been attenuated in some cases, but not completely resolved
- A very rare crash involving units with active mines attacking Hallucinating targets has been resolved
- A theoretical fix for a rare AI panic has been applied
- Hydralisks are now properly enabled by the Almak Antre
- Alkaj Chasms now properly require the Alaszil Arbor

# 28 December 2020

## General
- Our resident Santa, DarkenedFantasies, has supplied us with presents to replace our Mineral Fields!

## Bugfixes
- Fixed a function that handled AI broodspawn not returning when it should have, potentially resolving AI-related crashes.

## Maps
- The following community maps have been updated:
	- Breeding Grounds by Keyan / Dark Metzen (Scenario)
		- Extermination Ops no longer become an enemy of neutral resources
		- All Zerg players now begin with 250 minerals and 1 additional drone
		- All Zerg players no longer begin with zerglings, a spawning pool, or an additional overlord
		- Updated mission objectives to include thanks for Veeq
		- Less starting units/structures for Extermination Ops
	- Eclipsed by Keyan / Dark Metzen (Scenario)
		- Initial version
	- Oases by Keyan / Dark Metzen (Melee: 2v2v2v2)
		- Initial version

# 27 December 2020

## General
- Armor and weapon tooltips have now been dilineated into simple and robust (hold shift to see robust tooltips while mousing over)

## Bugfixes
- Additional egg-related AI crashes have been resolved (more may still exist)
- AI-controlled broodspawn now properly initialize their unit AI if the source Queen has died
- Vorvalings no longer benefit from Adrenal Surge when moving towards flying targets
- Infested Command Centers now have a button to select connected Larva
- Salvo Yard now has a unique damage overlay

## Terran
- New: Ion Cannon:
	- Powerful artillery structure with massive range; built from SCV; requires Science Facility

## Zerg
- New: Larval Colony:
	- Utility structure that generates Larva at 75% speed compared to Hatcheries; morphed from Creep Colony; requires Irol Iris

# 25 December 2020

## General
- Festive decorations have appeared on town centers and workers! Thanks to mao li for the Nexus reskin!

## Bugfixes
- Almak Antre now correctly enables Bactalisks from Hydralisks
- Broodlings and Broodlisks spawned from Incubators now correctly inherit the remove timer of their parent
- The Zoryusthaleth shadow has been corrected

## Protoss
- Simulacrum Autovitality:
	- Clones now inherit the HP of their origin

# 24 December 2020

## Caveats
Stability is not great still, due in large part to the egg rework. Stability is massively improved when not facing Zerg AI. Such crashes will be addressed very soon. Merry Christmas, and thanks for all the support during this crazy year!

## General
- Weapon and armor tooltips now show the following information, courtesy of Veeq7!
	- WEAPON:
		- Attack damage & Factor (now split from one another)
		- Attack cooldown
		- Targets
		- Splash radius
		- Splash type
	- ARMOR:
		- Status effects
		- Movement speed
		- Sight range
		- Movement type (Ground or Air)
		- Size (now color-coded)
- Starting melee workers spawn on the side of their town center closest to minerals
- Egg rallies have been temporarily disabled until multiselection and proper rally support is added
- Vespene Geyser, Refinery, Assimilator, and Extractor collision sizes have been standardized

## Editor
- Test map has been updated with all new units and structures (multiplayer test map WIP)
- JUNGLE:
	- Dirt/High Dirt Crevices have been appended to the other Crevice tile groups
	- Jungle (normal) Crevices have been added to the brush palette (more coming later)
- ICE:
	- Basilica center tiles have been added for high height levels
	- Veeq7's Christmas presents have been added

## Bugfixes
- Energy-related crashes (and some egg-related crashes) have been resolved
- Dark Swarm now functions as intended
- AI-controlled units revived by Apostles no longer freeze
- Archons and Augurs can now be martyred regardless of High Templar selection quantity
- High Templar can now be cancelled correctly
- Gorgoleths can now be morphed while the player is at their supply limit, in line with other morphs that do not involve a supply increase
- Broodlings are now correctly limited to spawning 2 Broodlings via Incubators
- Quarries and Future Stations can now be queued while the structure is lifted off
- Unpowered or disabled Crucibles no longer provide efficiency bonuses
- Plague now correctly applies Ensnaring Brood
- Protoss upgrades are now properly tied to completed structures of appropriate tiers
- Treasury's buttonset now correctly displays
- Keskathalors can now attack from within Bunkers
- Future Stations no longer have energy
- ICE: Basilica center tiles now have the correct height
- SCV explosions no longer fail to vanish if the SCV was killed during a revive from Lazarus Agent
- Egg shadows have been corrected (fixes silent errors and some visual inconsistencies)
- Medbay build tooltip has been corrected

## Zerg
- Queen Parasite:
	- now spawns 3x supply cost if the victim dies while on creep
- Defiler:
	- now morphs from Zoryusthaleth
	- now costs 25 minerals, 75 gas, 20 seconds (from 50/150/31.25)
- Defiler Mound:
	- now morphs from Zoryus Shroud (still requires Othstol Oviform)
	- now costs 75 minerals, 100 gas, 31.25 seeconds (from 100/100/37.5)
- New: Broodlisk:
	- Airborne swarmer that spawns from Parasited flyers
- New: Quazilisk:
	- Duelist strain that deals greater damage when attacking wounded targets; morphed from Vorvalings; requires Quazil Quay
- New: Quazil Quay:
	- Unlocks Quazilisk; morphed from Vorval Pond; requires Othstol Oviform
- New: Almaksalisk:
	- Assault flyer strain that ignites debuffs for extra damage; morphed from Bactalisks; requires Almak Antre
- New: Zoryusthaleth:
	- Frontline support strain that consumes corpses to heal organic allies; morphed from Larva; requires Zoryus Shroud
- New: Almak Antre:
	- Unlocks Almaksaleths from Bactalisks; morphed from Bactal Den; requires Othstol Oviform
- New: Zoryus Shroud:
	- Unlocks Zoryusthaleth; morphed from Drone; requires Irol Iris

# 20 December 2020

## General
- Several Zerg units and structures have been renamed, in an attempt to make the new suffixes more consistent with combat roles

## Bugfixes
- Ensnaring Brood no longer applies to buildings
- Gallery and Medbay build tooltips and Nuclear Silo requirement tooltip have been corrected
- Treasury build tooltip has been amended until its buttonset issue can be resolved
- Ion Cannon button has once again been removed

## Terran
- Armory:
	- New: Liftoff
		- Can now lift off, as with other Terran structures
		- **No animation yet**

## Zerg
- Eggs and Cocoons:
	- now only uses one egg for ground units and one cocoon for air units
	- HP now 150, from 200
	- armor now 4, from 10

# 18 December 2020

## Bugfixes
- A critical error with the extended sprite array has finally been worked around, fixing a long-standing and pervasive issue with regards to players desyncing
- Infested Terrans no longer display a deprecated passive icon (additionally fixes tooltip hover crash)
- Science Vessels can now cancel nuclear strikes

## Protoss
- Stargate:
	- Now costs 150/100, from 125/125
- Argosy:
	- Gas cost now 150, from 200

# 17 December 2020

## General
- Debug messages have been added that should print to screen during desyncs. If you encounter these messages, please screenshot and share them with your report!
- New weapon SFX have been added for Vassal, Manifold, Magister, Empyrean, and Kalkaleth
- Weapon SFX for the Nathrokor has been made shorter

## Editor
- ICE: Basilica blends for high snow and higher snow have been added

## Bugfixes
- Several edge case issues with data extenders have been resolved, fixing rare desyncs and crashes. This **does not** address the multiplayer vision desync.
- Simulacrums no longer crash when they or their attacker are killed while their passive is available
- Devourers can no longer be interrupted in the middle of their attack
- Nuclear Missiles launched by Science Vessels are correctly destroyed if the Vessel dies while painting
- ICE: Basilica height issues have been corrected

## Zerg
- Queen:
	- Broodspawn removed
	- Parasite energy cost now 75, from 100
	- Gamete Meiosis now instead grants +5 energy to any allied Queen within 192px (6 range) of broodspawn
	- New: Ensnaring Brood:
		- When activated, allied organics within 6 range apply Ensnare with their attacks. Ensnared targets move and attack 33% slower. Ensnaring Brood and Ensnare expire after 12 seconds.

## Protoss
- Pariah:
	- HP now 200, from 250
	- Now costs 300/250/60, from 400/400/62.5
	- Now moves roughly 20% faster

# 13 December 2020

## General
- New updaters from lucifirius and Milestone are now available! More robust solutions will eventually be deployed.
- All unit abilities are now available by default (i.e. no longer have tech requirements)
- Lobotomy Mine now sports a new (placeholder) graphic, wireframe (from jun3hong), and seeking SFX to distinguish it from Spider Mines
- Basilisk attack sound volume has been reduced
- Mineral Field wireframes have been updated, courtesy of DarkenedFantasies

## Editor
- ICE: Basilica has been ported from the Twilight tileset

## Bugfixes
- Saving the game no longer crashes
- Radiation Shockwave can no longer cause affected stats to overflow (fixes negative shields bug and energy overflow crash)
- Start locations now properly randomize (player colors are currently not in sync due to plugin conflicts)
- It is no longer possible for an active human slot to be converted to a computer slot in non-UMS game modes
- It is no longer possible to deploy more than two Lobotomy Mines per Ghost
- AI-controlled Corsairs now properly cast Disruption Web
- Lobotomy Mine stun and Ensnare slow durations now match their tooltips
- Lobotomy Mine wireframe and rank display has been corrected
- Medbay can now be built again

## Terran
- Armory:
	- Gas cost now 100, from 150
	- Can now lift off
- Future Station:
	- Gas cost now 50, from 100
	- Time cost now 30, from 32.5

## Zerg
- Iroleth:
	- now requires Iroleth Iris (previously named Overlord Iris)
- Othstoleth:
	- now requires Othstoleth Oviform (previously named Iroleth Quay)

## Protoss
- Vassal:
	- Now costs 75/0/1/18, from 50/0/1/20
	- Weapon damage now 5, from 8
	- Now moves 10% slower at top speed
	- New: Terminal Surge (passive):
		- Whenever a Vassal dies, robotic allies within 5 range gain 15% increased movement and attack speed. This bonus caps at 150%.
- Simulacrum:
	- HP now 40, from 45
	- Shields now 40, from 60
	- Size now Small, from Medium
	- Sight range now 7, from 8
	- Transport space now 1, from 2
	- Now costs 50/25/1/20, from 100/25/2/25
	- Weapon now deals 5+1 normal damage, from 10+1 explosive
	- Weapon range now 5, from 5.5
	- Weapon cooldown now 1.2, from 1.46
	- Revised: Autovitality (passive):
		- 5 seconds after taking damage from an enemy, the Simulacrum creates a copy of itself.
- Barghest:
	- Robotic Sapience passive removed
	- New: Repentent Chorus (passive):
		- Barghests gain 0.25 range for every allied Barghest within 5 range.
- Clarion:
	- Now costs 50 minerals, 100 gas, 30 seconds, from 50/125/31.25
	- No longer has a weapon
	- Now permanently channels Phase Link
- Aurora:
	- Passive damage now 20+2, from 50+3
	- Passive splash radius now 32/48/64, from 64/80/96
	- Passive splash type is now ally-safe

# 6 December 2020

## Editor
- SPACE PLATFORM: Low platform -> platform ramp extensions have been added, courtesy of DarkenedFantasies

## Bugfixes
- Certain projectiles have been made more consistent (i.e. less spin cases)
- Resources can no longer be locked down by Lobotomy Mines
- All resources now correctly regenerate to an upper limit of 5000
- Quarries and Pennants must now be completed in order to boost resource regeneration
- Pennants now correctly require a power field
- A hotkey conflict between the Valkyrie and the Wendigo has been resolved
- Devourer Haunt morph icon has been corrected
- Keskath Grotto now correctly enables Ultralisks
- Gorgoleths now correctly spend 5 energy per charged attack (from 10), no longer gain energy when attacking non-specialist units, and no longer benefit from a deprecated attack speed bonus
- Auroras now have a max search radius of 10 when looking for targets during Last Orders
- Augur morph icon and passive tooltip display have been corrected
- ICE: Some north-facing ramps have had their walkabilty and height corrected

## Terran
- Madcap:
	- HP now 70, from 80
	- Attack speed granted by Maverick Feeders passive now cools off by 20% for every second the Madcap spends walking
- Wraith:
	- Attack cooldown now 1.73, from 1.8
- Wendigo: 
	- HP now 80, from 120
	- Armor now 0, from 1
	- Mineral cost now 100, from 125
	- Time cost now 25, from 30
	- New SFX have been added for attack and death
- Gorgon:
	- Mineral cost now 175, from 200
	- Gas cost now 100, from 150
	- Time cost now 36, from 42.5
	- Top speed now 15 pixels per second, from 11
	- Acceleration rate and turn radius now slightly improved
	- Weapon now instead refreshes the timer of all slowing and stunning effects
- Pegasus:
	- Weapon cooldown for both weapons now 1.73, from 2
	- Anti-air weapon damage now 4x7, from 4x6
- Salamander:
	- Time cost now 30, from 35
	- Turn radius increased to better support precise micro
- Battlecruiser:
	- Time cost now 70, from 75
- Barracks:
	- Time cost now 45, from 50

## Zerg
- Creep:
	- now spreads twice as fast (recede speed to be changed soon)
- Nathrokor:
	- HP now 65, from 60
- Basilisk:
	- HP now 90, from 100
	- Gas cost now 50, from 25
	- Weapon range now 6, from 7
	- Weapon can now target air units
- Lurker:
	- attack damage now 15, from 20
	- attack cooldown now 3 seconds, from 2.46
	- projectile speed reduced by roughly 20%
	- weapon can now damage previously-damaged targets when returning to the Lurker
- Gorgoleth:
	- Attack cooldown now 0.53, from 1
	- *This is in response to a bugfix that removed an old passive*
- Guardian:
	- HP now 200, from 150
	- Gas cost now 50, from 100
	- Time cost now 20, from 25
- Lair:
	- now costs 50 minerals, 50 gas, 25 seconds, from 100/100/60
	- HP now 1600, from 1800
- Hive:
	- now costs 50 minerals, 50 gas, 25 seconds, from 150/150/70
	- HP now 2000, from 2500
- Sire:
	- now costs 50 minerals, 50 gas, 25 seconds, from 200/200/80
	- HP now 2500, from 3000

## Protoss
- Aurora:
	- Shields now 40, from 60
- Corsair:
	- HP now 80, from 100

# 22 November 2020

## Bugfixes
- A Grand Library damage overlay crash has been fixed (thanks to fagguto for reporting!)
- Warp textures no longer appear for a brief moment when Zerg structures finish construction
- Particle Accelerator now plays the correct "almost built" frame
- Iroleth and Kalkaleth now play the correct birth animations
- Marine, Madcap, and Robotics Authority frame overflows have been corrected

## Terran
- Cyprian:
	- Now moves roughly 15% slower
	- Safety Off toggle removed
	- Weapon now deals ally-safe splash damage in a 12/24/36 radius
	- Weapon now deals 8 explosive damage, from 9 normal damage
	- Weapon range now 6, from 5.5
- Shaman:
	- Now moves roughly 11% slower
	- Weapon removed
	- Now channels Administer Medstims permanently unless toggled
	- Now has a single toggle that switches between Medstims and Nanite Field
- Wyvern:
	- HP now 200, from 215
	- Weapon area damage now ally-safe
- Azazel:
	- Reads for Sublime Shepherd have been added
- Pegasus:
	- Anti-ground damage upgrade now 2, from 1

## Zerg
- Defiler:
	- Plague:
		- No longer requires anything
		- *This change was meant to have been implemented earlier.*
- Alkajelisk:
	- Base damage now 6x7, from 6x6
- Keskathalor:
	- Damage upgrade now 2, from 1

## Protoss
- Legionnaire:
	- Base damage now 2x6, from 2x5
	- Unified Strikes behavior removed
- Hierophant:
	- Signify: Malice:
		- Now applies to any target that sustains 5 Hierophant attacks
- Vassal:
	- Now costs 50 minerals, from 75
	- Shields now 20, from 30
- Manifold:
	- New passive: Phase Rush:
		- Gain 150% increased movement speed when moving towards any allied robotic unit within 8 range
- Clarion:
	- HP now 50, from 40
	- Shields now 50, from 60
	- New passive: Harmony Drive:
		- Gain up to 150% increased movement speed and acceleration based on shields
	- Base movement speed and acceleration now 75% of previous values 
- Exemplar:
	- Reads have been added to show when Khaydarin Eclipse is active
- Carrier:
	- Max Interceptor count now 12

# 15 Nov 2020

## Caveats
- A harmless but annoying crash often occurs when closing the game; we are still triaging this issue
- AI-controlled Ghosts do not care if they are out of Lobotomy Mines and will launch them anyway; will be fixed in a micropatch

## Maps
- Unit settings have been refreshed on all maps
- An early draft of "Into the Flames", Protoss map 2, is now available

## Misc
- Two emojis have surfaced within the font files, courtesy of DarkenedFantasies!

## Bugfixes
- Two bullet and overlay overflow crashes have been resolved
- Shaman Nanite Field's disabled string now displays correctly
- Savants no longer crash the game upon reviving from Lazarus Agent, though their animation is still jank
- Ion Cannon debug button has been removed
- Pariah, Empyrean, Star Sovereign, and Ultralisk tech requirement errors have been resolved
- Cosmic Altar and Arbiter Tribunal button placements have been corrected
- Several Protoss stellar tech structures have had their tooltips corrected
- Atreus unit responses have been corrected (still using Dragoon responses as a placeholder)
- Augur debug messages have been disabled
- Idols no longer create multiple shadows after certain animations are played
- Clarion SFX errors have been fixed
- Architects no longer attack more times than intended while targeting air units
- Exemplar and Clarion abilities no longer freeze the units if activated while moving
- Star Sovereign's Grief of All Gods tooltip no longer shows an energy cost
- Crucibles now enhance allied and non-Protoss structures
- Sires now correctly generate Larva
- Multiple Mutalisks can once again be ordered to mutate simultaneously

## AI
- Fix anachronistic responses to air and cloak rushes
- Improve AI upgrade logic and a few build orders
- Add AI spellcasting logic for the following, courtesy of iquare:
	- Apostle Lazarus Agent
	- Savant Power Siphon
	- Queen Parasite
	- Clarion Phase Link
	- Star Sovereign Grief of All Gods

## Terran
- Wendigo
	- Now costs 125 minerals, 25 gas, 30 seconds, from 150/50/32.5
- Wraith
	- Now costs 100 minerals, 75 gas, 32.5 seconds, from 150/100/37.5
	- Now attacks all targets with Burst Lasers
	- Weapon now deals 3*(3+1) concussive damage, from 8+1 normal damage
	- Attack cooldown now 1.8, from 2; Capacitors behavior removed
- Gorgon
	- Attack cooldown now 1.46, from 2

## Protoss
- Ecclesiast
	- Damage now 10, from 12
	- Damage type now explosive, from normal
- Idol
	- No longer deals splash damage to allies
- Pylon, Crucible, Pennant
	- Added passive icon

## Zerg
- Overlord
	- Now moves at a slower movespeed (still ~3x faster than vanilla)
	- Can now transport units by default
	- Transport capacity now 6, from 8
- Othstoleth
	- Now moves at a faster movespeed
- Spire
	- Now costs 100 minerals, 150 gas, from 150/100

# 8 November 2020

## Preamble
This is by far and away the largest patch in Project: HYDRA's history. It also accompanies the launch of [our website](http://fraudsclub.com/hydra). This patch is a new foundation for HYDRA and once enough bugs have been squashed and rough edges have been sanded off, we will be dropping the 'beta' suffix and considering the project a full release.

This does not mean I am not committed to continuing to update the project once it reaches this threshold, nor does it mean that the project will be without (sometimes obvious) flaws that will need time to be corrected, but it's an important moment in the project's development nonetheless. 

This update is the product of my year-long streak of daily modstreams that concluded on 3 Nov 2020 and would not be possible without the time and dedication of cawtributors Veeq7, iquare, DarkenedFantasies, and Baelethal. An additional shoutout to those who provided feedback during streams and who have tested down the years is also warranted. Thanks to everyone who helped to make this project what it is today.

**Please note:** some in-game ability and passive tooltips may disclose information that is currently inaccurate. Almost all of these describe intended functionality, and the game state will be brought in line with these descriptions as time goes on. Another thing to note is that AI support is rather rough around the edges and will be improved shortly.

## Bugfixes
- Building unit responses now play correctly
- Missing weapon names have been filled in
- Madrigal weapon 2 now displays in the unit info panel; range and projectile speed are now updated correctly
- Science Vessel Observance no longer casts when the ability button is activated
- Centaur secondary weapon now displays in the unit info panel
- Incorrect requirement strings for Photon Cannon, Shield Battery, and Protoss ground upgrades have been corrected
- Several passive tooltips have been amended and new ones have been added
- Hotkey conflicts have been resolved at the Covert Ops

## General
- Tiered upgrades have been centralized and now effect all units of each race
- Several new wireframes have been added, courtesy of jun3hong!
- Many custom unit and building graphics have been reprocessed to improve team color and aesthetics
- Bouncing attacks will no longer fail to bounce if the attacker becomes stunned during the projectile's travel time

## Terran
- Marine Stim Pack, Vulture Spider Mines, and Wraith Cloaking Field:
	- no longer require anything
- Firebat
	- now has death / corpse animations
- Medic Optical Flare:
	- now requires Engineering Bay
- Shaman:
    - no longer slows units with its attack
	- New: Nanite Field:
		- Shaman ability that heals mechanical allies for 4 HP per second; requires Science Facility
- Savant Energy Decay:
	- now a passive on-hit behavior called Entropy Gauntlets; now expires after 4 seconds
- Madrigal:
	- weapon 1 now fires one missile per nearby target
- Azazel Defensive Matrix:
	- now cleanses all debuffs on cast
- Science Vessel:
	- Irradiate:
		- now also procs Radiation Shockwave after 4 seconds
		- shockwave deals 100 damage to shields and energy, from infinite
	- Observance:
		- now shows overlay on victims
		- now shows overlay on any Science Vessel in range of a tagged target
- Centaur:
	- primary weapon range now 7, from 6
	- secondary weapon no longer deals area damage and now follows targets
- Battlecruiser:
	- now trained at Nanite Assembly; now costs 75 seconds, from 83.333
- General (structures):
	- several structures have been rearranged between Basic and Advanced
- Comsat Station:
    - now requires Armory
- Medbay, Nuclear Silo:
	- now require Engineering Bay
- Academy and Drydock
	- removed
- Armory:
	- HP now 750, from 700
	- now costs 150 gas, from 100
	- now exclusively researches tiered upgrades
- Future Station:
	- now costs 100 gas, from 50
	- now costs 32.5 seconds, from 25
	- now requires Engineering Bay, from Science Facility
	- now exclusively built at Armory
- Treasury:
	- now adds +2 supply to nearby structures
	- effect range now 10, from 8
- New: Madcap:
	- Mechanized infantry with a stand-your-ground attitude; trained at Barracks; requires attached Gallery
- New: Apostle:
	- Advanced healing specialist that can reanimate allied organics; trained from Barracks, requires attached Medbay, enhanced by Science Facility
- New: Cyclops:
	- Light walker; trained at Factory
- New: Wendigo:
	- Melee fighter craft; built from Starport
- New: Gorgon:
	- Well-rounded corvette; built from Starport; requires attached Munitions Bay
- New: Pegasus:
	- Destroyer with specialized anti-ground and anti-air capabilities; trained at Staport; requires attached Physics Lab
- New: Salamander:
	- Destroyer equipped with an incendiary payload; built from Nanite Assembly
- New: Phobos:
	- Ultimate capital ship with unmatched firepower; trained at Nanite Assembly; requires attached Particle Accelerator
- New: Penumbra:
	- Ultimate walker whose cannon pierces through all targets in a line; trained at Nanite Assembly; requires attached Particle Accelerator
- New: Quarry:
	- Addon for Command Center and Treasury; boosts nearby resource regeneration; requires Science Facility
- New: Nanite Assembly:
	- Advanced Terran production; requires Science Facility
- New: Particle Accelerator:
	- Addon that enables Penumbra and Phoboses; built at Nanite Assembly

## Protoss
- Shuttle:
    - now trained at Nexus
    - now requires Forge
- Witness:
    - formerly known as Observer
    - now trained at Nexus
    - now requires Forge
- Dragoon:
    - now requires nothing
- Legionnaire:
	- now requires Principality, from Forge
- Hierophant:
    - now requires Cybernetics Core, from Ancestral Effigy
- High Templar:
	- now trained at Grand Library
	- no longer requires anything
	- now costs 125 gas, from 150
	- now costs 30 seconds, from 31.25
- Dark Templar:
	- now trained at Grand Library
	- no longer requires anything
	- now costs 100 minerals, from 125
	- now costs 30 seconds, from 31.25
- Archon:
	- now requires Templar Archives
- Dark Archon:
	- now requires Templar Archives
	- Mind Control:
		- can now be cancelled to rescind control of all affected targets
	- Maelstrom:
		- no longer requires anything
		- now fires a projectile instead of being instant-cast
- Simulacrum:
	- no longer requires anything
	- now benefits from Autovitality whenever a nearby robotic unit dies
- Reaver, Clarion:
	- now trained at Robotics Authority
	- no longer require anything
- Corsair, Panoptus:
    - no longer require anything
- Exemplar:
    - now requires Stellar Icon
	- movement speed now 1413, from 1707 (~21% reduction)
    - can no longer attack air units
- Empyrean:
    - now trained at Argosy
    - now requires Celestial Shrine
	- projectile now moves at 40 pixels per second, from 80
	- weapon now splashes in a small radius
- Arbiter:
	- no longer fails to acquire targets if the owner is an AI player
	- Recall:
		- channel time now 3 seconds, from ~1.5
		- now prevents any additional orders from being given during its cast effect
- Pylon:
	- no longer boosts research speed of Forges
- Forge:
	- now requires Nexus, from Gateway
	- now located in Core Structures
- Cybernetics Core, Astral Omen, and Stellar Icon:
	- no longer research upgrades
- Grand Library:
	- now acts as advanced Templar production
	- now requires Gateway
- Automaton Register:
	- placement size now 4x3, from 2x3
- Machinist Hall:
    - formerly known as Observatory
- Arbiter Tribunal:
    - now requires Astral Omen
- Cosmic Altar:
    - now requires Arbiter Tribunal
- New: Ecclesiast:
	- Supportive ranged infantry; trained at Gateway
- New: Amaranth:
	- Stalwart skirmisher; trained at Gateway; requires Citadel of Adun
- New: Atreus:
	- Interminable strider; trained at Gateway; requires Ancestral Effigy
- New: Augur:
	- Ultimate martyr; morphed from High Templar; requires Sanctum of Sorrow
- New: Pariah:
	- Profane mechanized heretic; trained at Grand Library; requires Sanctum of Sorrow
- New: Vassal:
	- Swarming robotic flyer; trained at Robotics Facility
- New: Idol:
	- Ranged robotic walker; trained at Robotics Facility; requires Automaton Register
- New: Golem:
	- Robotic frontliner; trained at Robotics Facility; requires Machinist Hall
- New: Barghest:
	- Profane swarming flyer; trained at Robotics Authority; requires Strident Stratum
- New: Architect:
    - Profane unstable artillery; trained at Robotics Authority; requires Synthetic Synod
- New: Gladius:
	- Crowd-controlling corvette; trained at Stargate; requires Astral Omen
- New: Magister:
	- Siege platform; trained at Argosy
- New: Star Sovereign:
	- Ultimate capital ship with unmatched firepower; warped from Argosy; requires Cosmic Altar
- New: Pennant:
	- Support structure that boosts resource regeneration rate; located in Core Structures; requires Forge
- New: Crucible:
	- Support structure that boosts efficiency of nearby structures; located in Core Structures; requires Forge
- New: Principality:
	- Enables Legionnaires; located in Templar Structures; requires Gateway
- New: Sanctum of Sorrow
	- Enables Terminuses; located in Templar Structures; requires Blazing Effigy
- New: Robotics Authority
	- Advanced robotic production; located in Robotic Structures; requires Robotics Facility
- New: Argosy:
	- Advanced spacecraft production; located in Stellar Structures; requires Stargate

## Zerg
- Nathrokor:
	- weapon damage now 6, from 5
	- weapon cooldown now 0.8 seconds, from 1.2
	- movement speed now 18 pixels per second, from 16 (a 112.5% buff)
	- new behavior: Adrenal Frenzy:
		- Whenever the Nathrokor kills a target, all allied organics within 3 range gain 150% movement speed and acceleration for 5 seconds.
- Gorgoleth:
	- now costs 75 minerals, from 50
	- movement speed now 1707, from 1420
	- Essence Drain now a passive on-hit behavior that drains energy equal to damage dealt when attacking specialists
- Queen:
	- now costs 150 minerals, 100 gas, 32.5 seconds, from 100/75/31.5
- Guardian:
    - now morphed from Gorgoleth
	- damage now 10, from 20
	- Searing Acid (passive):
		- projectiles gain up to 300% damage based on distance travelled
- Guardian Roost:
    - now morphed from Gorgoleth Apiary
- New: Kalkaleth:
	- Crowd-controlling fighter strain; morphed from Nathrokor; requires Kalkath Bloom
- New: Alkajelisk:
	- Frontal destroyer strain; morphed from Ultralisk; requires Alkaj Chasm
- New: Keskathalor:
	- Siege strain; morphed from Ultralisk; requires Keskath Grotto
- New: Kalkath Bloom
	- Enables Kalkaleths from Nathrokors; morphed from Nathrok Lake; requires Iroleth Quay
- New: Alkaj Chasm:
	- Enables Alkajelisks from Ultralisks; morphed from Ultralisk Cavern; requires Othstoleth Berth
- New: Keskath Grotto:
	- Enables Keskathalors from Ultralisks; morphed from Ultralisk Cavern; requires Iroleth Quay

# 4 September 2020

## Bug Fixes
- Fix several incorrectly-configured shadows (thanks DarkenedFantasies!)

## Editor
- Updated mpq and raw AI files to use latest data
- Jungle: Add stacked raised jungle tiles
- Jungle brush: Add jungle mud and stacked raised jungle

## Zerg
- Nathrokor and Gorgoleth unit handling has been improved
- Spawning Pool build time now 31.5 seconds, from 50
- Hydralisk Den build time now 31.5 seconds, from 20
- Spire build time now 31.5 seconds, from 50

# 30 August 2020

### Caveats
- We have identified issues with the melee AI's defense functions, which result in AI ignoring attack forces for prolonged periods of time - this will be resolved next patch
- Energy Decay (Savant) does not yet correctly damage energy expenditures in all cases
- Using Phase Link (Clarion) and Khaydarin Eclipse (Exemplar) while the unit is moving can sometimes freeze the unit

### Global
- All melee AI have been updated to better suit the techtree changes from this patch and the 20 August patch (expect wonky performance in certain cases)

### Bug Fixes
- Several missing tooltips have been added (resolves obscure null mouseover crashes)
- Shaman now correctly displays its rank when selected (actual rank name is incorrect and will be updated soon)
- Future Station now builds at the Drydock (addon position is still not correct); Addon position now fixed for Academy and Armory
- Morphing various Zerg units no longer overflows used supply (also resolves eventual crash from large overflow)
- Sunken Colony no longer attacks up to 2 targets (holdover from Tendril Amitosis upgrade)
- Iroleths now correctly boost the regeneration of occupied Vespene Geysers
- Malice (Hierophant) and Malediction (Dark Archon) overlay colors have been corrected

### Terran
- Southpaw attack angle now 64, from 16 (significantly improves patrol-micro capabilities)
- Ship upgrade hotkeys (at the )Drydock) are now identical to vehicle upgrade hotkeys (Armory)
- New: Savant - Anti-specialist infantry that siphons energy from other specialists; trained from Barracks, requires attached Covert Ops, enhanced by Science Facility

### Zerg
- Spawning Pool, Hydralisk Den, Spire, Evolution Chamber, Sunken Colony, Spore Colony, Iroleth Quay, and Overlord Berth now require nothing
- Mutation Pit, Basilisk Den, Lurker Den, Gorgoleth Apiary, Devourer Haunt, and Queen's Nest now require Overlord Iris
- Guardian Roost, Nydus Canal, Ultralisk Cavern, and Defiler Mound now require Iroleth Quay
- New: Nathrokor - Morphed from Zergling, fast close-range flyer, requires Nathrok Lake
- New: Nathrok Lake - Morphed from Spawning Pool, enables and enhances Nathrokors; requires Lair

### Protoss
- Protoss buildings have been sorted into distinct command cards, and the Probe's command card hotkeys have been rearranged as a result
- Legionnaire now trained at Gateway; now requires Forge
- Hierophant now trained at Gateway; now requires Blazing Effigy
- Observer now requires Observatory
- Panoptus now costs 250 minerals, from 275; now requires Stellar Icon
- Corsair now costs 150 minerals, 75 gas, from 150/100; now requires Astral Omen
- Carrier now has 250 shields, from 150
- Cybernetics Core now researches ground upgrades (Forge remains unchanged)
- Observatory has returned; now costs 100 minerals, 75 gas, 31.25 seconds, from 50/100/18.75
- Synthetic Synod (formerly Robotics Support Bay) now costs 250 minerals, 150 gas (from 200/150); now requires Automaton Register
- Fleet Beacon now requires Celestial Shrine and no longer researches ship upgrades
- Arbiter Tribunal now requires Cosmic Altar
- Plasma Shields base cost now 150 minerals, 150 gas, from 200/200, and factor cost now 75 minerals, 75 gas, from 100/100
- New: Simulacrum - Warped from Robotics Facility, swarming combat drone that gains increased movement and attack speeds whenever a nearby robotic unit dies; requires Automaton Register
- New: Clarion - Warped from Robotics Facility, nimble support drone that offers positional shield-based utility; requires Strident Stratum
- New: Exemplar - Warped from Stargate, powerful corvette with high damage potential; requires Exalted Ashlar
- New: Empyrean - Warped from Stargate, frontal destroyer with high single-target damage; requires Cosmic Altar
- New: Blazing Effigy - Warped from Probe, enables Hierophants, requires Cybernetics Core
- New: Automaton Register - Warped from Probe, enables Simulacrums, requires Robotics Facility
- New: Strident Stratum - Warped from Probe, enables Clarions, requires Observatory
- New: Stellar Icon - Warped from Probe, researches ship upgrades and enables Panoptuses, requires Stargate
- New: Astral Omen - Warped from Probe, researches ship upgrades and enables Corsairs, requires Stargate
- New: Celestial Shrine - Warped from Probe, enables Exemplars, requires Astral Omen
- New: Cosmic Altar - Warped from Probe, enables Empyrean, requires Celestial Shrine

# 20 August 2020

### Global
- Passive icons have been added to all applicable units
- Passive descriptions have been made accurate to the latest game state

### Terran
- Cyprian supply cost now 1, from 2
- Shaman now costs 100 minerals, 50 gas, from 100/75
- Madrigal Tempo Change icon now changes upon activation
- Dropship acceleration now 22, from 17 (~125%), and now unloads at 125% speed, increasing to 150% speed when below half health
- Valkyrie supply cost now 2, from 3; now requires attached Munitions Bay
- Wyvern now requires attached Munitions Bay
- Science Vessel now requires attached Control Tower; Observance now requires Science Facility
- Battlecruiser weapon range now 7, from 6
- Physics Lab now costs 100 minerals, 50 gas, from 75/50
- Armory now costs 150 minerals, 100 gas, from 125/75
- Drydock now costs 100 minerals, 150 gas, from 125/75
- New: Munitions Bay - addon for Starport, enables Valkyries and Wyverns; costs 75 minerals, 50 gas, 20 seconds; requires Engineering Bay

### Zerg
- Sunken and Spore Colonies now require Hatchery, from Evolution Chamber
- Spire no longer researches flyer upgrades, and now costs 150 minerals, 100 gas, from 200/150
- Evolution Chamber now researches flyer upgrades, and now costs 125 minerals, 50 gas, from 75/0
- Mutation Pit now researches flyer upgrades

### Protoss
- Shuttle acceleration now 26, from 17 (~150%)
- Corsair Disruption Web now attaches to the caster, now costs 25 energy to activate, and now drains energy while active
- Photon Cannon now costs 37.5 seconds, from 31.25, and now require Nexus

### Caveats
- Disruption Web activation does not spend 25 energy as it should, and does not yet disable air weapons (including the casting Corsair)
- Protoss AI have not yet been updated to use the latest Disruption Web implementation
- Terran AI have not yet been updated to use Munitions Bays and will fail to build Wyverns and Valkyries

# 19 August 2020

### Global
- Drop menu timer now ticks down nearly instantly
- F1 and F5-F8 are now usable as camera hotkeys; F1 no longer opens the help menu
- 1/256 "miracle" miss chance has been removed
- Abilities no longer require researching, and instead require tech structures, if they require anything at all
- Unit-specific upgrades have been rolled into default unit stats, either partially or fully, and otherwise have been removed; specialists no longer gain +50 energy
- DarkenedFantasies's Terran geyser is now present in Installation and Platform tilesets, and his modified Jungle and Ice geysers are also present
- Vespene Geyser shadow gaps on Desert and Ice tilesets are now fixed

### Terran
- SCV Fusion Sealers removed
- Marine weapon range now 5 (from 4); Stim Packs now require Academy; U-238 Shells removed
- Firebat HP now 60 (from 55) no longer uses Stim Packs and benefits from Apollyoid Adhesive by default
- Cyprian can now activate Safety Off by default; Plasteel Servos removed
- Ghost now moves at 125% speed while cloaked; Somatic Implants removed; Lockdown now requires Science Facility and costs 75 energy (from 100)
- Medic no longer requires attached Medbay, no longer benefits from Adrenaline Packs, now costs 1 supply (from 2), and now has 60 HP (from 70); Optical Flare now requires Medbay
- Shaman now benefits from Adrenaline Packs and Overcharge by default
- Vulture top speed now 2026 (from 1707, ~120%); Ion Thrusters removed; Spider Mines now require Armory
- Southpaw now benefits from Haymaker Battery by default
- Siege Tank (Tank Mode) armor now 2 (from 1); Babylon Plating and Hammerfall Shells removed
- Goliath anti-air range now 6, from 5; Charon Boosters removed
- Madrigal can now activate Tempo Change by default; Requiem Afterburners removed
- Wraith now benefits from Capacitors by default; Cloaking Field now requires Drydock
- Dropship now benefits from Hotdrop by default; Passenger Insurance removed
- Wyvern can now activate Reverse Thrust by default; turn rate now 20 (from 18)
- Valkyrie turn rate now 20 (from 18)
- Azazel now benefits from Innervating Matrices by default; Sublime Shepherd now requires Science Facility
- Science Vessel now benefits from Radiation Shockwave and Peer-Reviewed Destruction by default; Observance now available by default
- Centaur now benefits from Sunchaser Missiles by default
- Battlecruiser now benefits from Fore Castle by default; Yamato Gun now available by default; Umojan Batteries removed
- Missile Turret Sequester Pods removed
- Comsat Station Odysseus Reactor removed
- Nuclear Silo now requires Science Facility
- Nuclear Missile now benefits from Ground Zero by default; Augustgrad's Revenge removed
- New: Skyfall - Paladin ability, fires 20 missiles at target site; requires Science Facility

### Zerg
- Zergling now moves at 125% movement speed and gain up to 133% attack speed by default; Metabolic Boost and Adrenal Glands removed
- Vorvaling now benefits from Adrenal Surge by default, and now have Captivating Claws, which applies a 25% movement speed slow on hit that lasts 1 second (can stack up to 4x, 1 per unique vorvaling)
- Hydralisk now benefits from Muscular Augments by default; Grooved Spines removed
- Basilisk now benefits from Caustic Fission and Corrosive Dispersion by default
- Lurker now benefits from Seismic Spines by default; Tectonic Claws removed
- Mutalisk now costs 75 gas (from 100); Ablative Wurm and Lithe Organelle removed
- Queen now benefits from Gamete Meiosis by default; Parasite now available by default
- Broodling now benefits from Incubators by default, and no longer benefit from Ensnaring Brood
- Infested Terran now benefits from Ensnaring Brood by default
- Devourer now benefits from Critical Mass by default
- Gorgoleth now benefits from Ravening Mandible by default; Essence Drain now available by default, and now caps at 75 energy (from 50)
- Defiler now benefits from Metastasis by default; Consume is now available by default, Plague now requires Sire
- Ultralisk now costs 250 minerals, 250 gas, 5 supply (from 200/200/4), and now benefits from Saber Siphon and Kaiser Rampage by default
- Hatchery now costs 275 minerals (from 300)
- Creep Colony Rapid Reconstitution removed
- Sunken Colony Tendril Amitosis removed
- Nydus Canal now benefits from Nydal Transfusion by default

### Protoss
- Zealot now moves at 125% movement speed by default; Leg Enhancements removed
- Legionnaire now moves at 150% movement speed and benefits from and Blade Vortex by default
- Dragoon now benefits from Singularity Charge by default; Taldarin's Grace removed
- Hierophant weapon range now 4.5 (from 3.5), and now benefits from Signify: Malice by default
- Dark Templar now moves at 112% movement speed, and benefits from Silent Strikes by default
- High Templar now benefits from Ephemeral Blades and Ascendancy by default; Psionic Storm now available by default
- Dark Archon now requires Templar Archives and benefits from Malediction by default; Maelstrom now requires Grand Library
- Archon now benefits from Astral Aegis by default
- Shuttle top speed now 1416 (from 1133, ~125%); Gravitic Thrusters removed
- Reaver now remains at full Scarabs at all times; Reinforced Scarabs and Relocators removed
- Observer sight range now 10 (from 9); Sensor Array and Gravitic Boosters removed
- Aurora now benefits from Last Orders by default
- Panoptus now requires Fleet Beacon, and now benefits from Gravitic Thrusters by default
- Carrier now benefits from Auxilliary Hangars by default; Skyforge removed
- Arbiter Recall and Stasis Field now available by default; Khaydarin Core removed
- Pylon now benefits from Power Flux by default; Temporal Batteries removed
- Photon Cannon Prismatic Shield removed

# 17 August 2020

### Terran
- Southpaw now has accurate acquisition range, now benefits from Knockout Drivers by default, and now costs 75 minerals (from 125)
- Madrigal weapon 1 now deals 15 damage, from 14, weapon 2 now has 10 range, from 9, and now costs 125 minerals, from 100
- Engineering Bay now costs 40 seconds, from 42.75
- Factory now costs 175 minerals, 50 gas (from 200/100)
- Starport now costs 100 minerals, 150 gas, 50 seconds (from 150/100/43.75)
- Treasury now boosts supply generation by 1 for all structures within 10 range (this bonus can stack)
- Science facility now costs 70 seconds (from 75)
- Quantum Institute now costs 900 minerals, 900 gas, 100 seconds (from 800/800/90)
- All production addons now cost 20 seconds (from 25)
- Covert Ops now costs 75 gas (from 50)
- Machine Shop now costs 100 minerals (from 75)
- Salvo Yard now costs 75 gas (from 50)
- Control Tower now costs 75 minerals (from 50)
- Physics Lab now costs 75 gas (from 50)
- New: Haymaker Battery upgrade - +1 Southpaw range
- New: Requiem Afterburners upgrade - +150% Madrigal missile speed and acceleration

### Zerg
- Spire now costs 50 seconds (from 75)
- Basilisk Den now requires Hatchery, from Lair, and costs 75 gas (from 100)

### Protoss
- Aurora now has 60 HP (from 80)
- Pylon now provides 7 supply, +1 per Nexus (from 8)
- Robotics Facility now costs 200 minerals, 100 gas (from 150/150)
- Stargate now costs 125 minerals, 175 gas, 50 seconds (from 150/150/43.25)
- Grand Library now costs 300 minerals, 450 gas, 75 seconds (from 900/900/90)

# 15 August 2020

### Global
- A massive techtree reshuffle has occurred for all races, and AI builds have been significantly reworked as a result (expect bugs)
- Weapon and cast ranges are now increased if targeting a lower height level (+2 range per difference)
- Armor is now increased if taking damage from a lower height level (+1 armor per difference)
- Miss chance when targeting a higher height level has been removed

### Terran
- Siege Tanks now require 8 transport space in Siege Mode
- Siege Mode now researched by default
- Paladin attack offset has been adjusted and missile speed has been slightly increased
- Dropship now requires nothing
- Nuclear Silo now requires Academy and Engineering Bay
- Factory and Starport now require Command Center
- Academy now researches infantry upgrades in addition to Marine upgrades and can build Future Stations
- Engineering Bay now costs 200 minerals, 200 gas, no longer researches infantry upgrades and can no longer build Future Stations
- Missile Turret now requires Command Center and requires 6 transport space
- Machine Shop and Salvo Yard now require Engineering Bay and no longer research Vulture or Southpaw upgrades
- Armory now requires Factory, no longer researches ship upgrades, and now researches Vulture and Southpaw upgrades in addition to vehicle upgrades
- Control Tower now requires Engineering Bay and no longer researches Wraith upgrades
- Science Facility now costs 650 minerals, 650 gas, 75 seconds and requires Engineering Bay
- Tiered upgrades now require Engineering Bay for level 2
- New: Treasury - Built from SCV, resource drop-off point that can build Command Center addons; requires Engineering Bay
- New: Drydock - Built from SCV, researches ship, Wraith, and Dropship upgrades, and can build Future Stations; requires Starport
- New: Quantum Institute - Built from SCV, enables end-tier tech; requires Science Facility
- Vorvaling now costs 25 minerals, 12 seconds (was 25/25/13)
- Gorgoleth handling, top speed, and weapon range have been slightly improved
- Sunken Colony now requires Evolution Chamber
- Creep, Sunken, and Spore Colonies now require 6 transport space (known issue: Colonies don't spread creep upon unload)

### Zerg
- Hydralisk Den now requires Hatchery
- Basilisk Den now costs 125 minerals, 100 gas, 42.5 seconds (was 150/100/37.5)
- Lurker Den now costs 150 minerals, 200 gas, 42.5 seconds (was 100/150/75)
- Gorgoleth Apiary now costs 150 minerals, 100 gas, 42.5 seconds (was 200/150/75)
- Devourer Haunt now costs 200 minerals, 50 gas, 42.5 seconds (was 150/50/32.5)
- Iroleth Quay now costs 350 minerals, 350 gas, 42.5 seconds (was 150/100/37.5) and morphs from Overlord Iris
- Kaiser Rampage now requires Sire
- New: Iroleth - Morphed from Overlord, grants 4 extra supply and transport space and boosts nearby resource regeneration; requires Iroleth Quay
- New: Othstoleth - Morphed from Iroleth, grants 4 extra supply and transport space, and grants 150% increased morph speed to nearby eggs; requires Othstoleth Berth
- New: Overlord Iris - Morphed from Drone, enhances Overlord movement speed, upgrades Overlords, and enables Lairs; requires Hatchery
- New: Othstoleth Berth - Morphed from Iroleth Quay, enables Othstoleths and Sires; requires Hive
- New: Sire - Morphed from Hive, enables end-tier tech; requires Othstoleth Berth

### Protoss
- Dark Templar now requires Citadel of Adun
- Archon and Dark Archon now require Grand Library
- Archons now have 450 shields, up from 350, and move slightly faster
- Forge no longer researches utility upgrades
- Photon Cannon now requires Robotics Facility, and now has 125 HP, 125 shields, 1 armor (was 100/100/0)
- Cybernetics Core now costs 150 minerals and no longer researches ship upgrades
- Robotics Facility now costs 150, minerals, 150 gas (was 200/200) and now requires Nexus
- Stargate now requires Nexus
- Robotics Support Bay now costs 200 minerals, 150 gas, 32.5 seconds (was 150/100/18.75) and researches infrastructure-related utility upgrades
- Fleet Beacon now costs 200 minerals, 100 gas (was 300/200) and researches ship upgrades
- Observatory now called Wisdom Altar; costs 150 minerals/100 gas, enables and upgrades Clarions, and researches shield-related utility upgrades
- New: Grand Library - Warped from Probe, enables Archons and end-tier tech; requires Templar Archives

# 13 August 2020

### Global
- Remove waitrands at the top of several iscript blocks, ruling iscript out as a cause for desyncs

### Terran
- Resolve hotkey conflicts (Cyprian/Firebat, Shaman/Medic)
- Veeq7: Optimize AI training blocks to better utilize new units

### Zerg
- Implement Iroleth Quay
- Spire now requires Hatchery, from Evolution Chamber
- Hive now requires Iroleth Quay, from Queen's Nest
- Queen's Nest now costs 150/150, from 150/100
- Queen now costs 100/75, from 100/100

# 12 August 2020

### Global
- Adjust turn rates of several air units
- First pass at adjusting AIs to use new units and abilities

### Terran
- Overhaul addon system, escalations removed
- Add Paladin - slow siege walker with powerful missile salvo attack, trained at Factory
- Add Madrigal - nimble hovertank with weapon switch upgrade, trained at Factory
- Add Southpaw - hit and run craft, with slow stacks per attack upgrade, trained at Factory
- Add Centaur - destroyer, with secondary weapon upgrade, trained at Starport
- Add Gallery addon - unlocks Firebats and Cyprians
- Add Salvo Yard addon - unlocks Madrigals and Paladins
- Add Babylon Plating - +1 armor upgrade for Siege Tanks while in Tank Mode
- Science Vessel now requires attached Physics Lab
- Defensive matrix is no longer bleeding into hp

### Zerg
- Add Gorgoleth - melee flyer that consumes energy to deal area damage, morphed from Mutalisk
- Overhaul Broodspawn - can now be cast on terrain, spawns more broodlings when used on creep, deals no damage
- Overhaul Broodling, more swarmy in general, shorter life
- Overhaul Parasite, affected units now spawn broodlings on death
- Add Incubators, allows Broodlings to spawn more Broodlings on kill

### Protoss
- Rework Scout into two distinct units
- Add Aurora - light fighter craft specialized at skirmishing
- Add Panoptus - corvette that fires anti-matter missiles at all targets
- Overhaul Hallucination - now tags an enemy, creating one Hallucination per attacker
- Carriers now spawn with 8 interceptors, when capacity is researched
