# Project: Hydra, Melee Maps by Veeq7

This repository contains all melee maps I have made for Project: HYDRA.
On my [youtube channel](https://www.youtube.com/channel/UCZcHu6iW-KRbOUNzn2yA8NA) you can watch gameplay of those maps.

# What is Project: HYDRA

>Project: HYDRA is a comprehensive overhaul of Starcraft: Brood War, for version 1.16.1. It features revisioned AI and techtrees in an effort to design an experience meaningfully removed from what is offered by the 20+ year old experience designed by Blizzard.

Project: Hydra is currently in development phase. It is daily developed by Pr0nogo and his cawtributors.

### Project: HYDRA Links

* [Project Page](https://fraudsclub.com/hydra/)
* [Project Repo](https://gitlab.com/the-no-frauds-club/release/HYDRA-beta)
* [Pr0nogo's Youtube](https://www.youtube.com/user/Pr0nogo)
* [Old Wiki Page](http://pr0nogo.wikidot.com/sc-hydra)
