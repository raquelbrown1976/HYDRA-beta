# Project: HYDRA

Thank you for testing Project: HYDRA 2.0!

Please send any feedback to me through [my discord server](https://discordapp.com/invite/s5SKBmY), [my forum](https://thenofraudsclub.proboards.com), or [my email](mailto:Pronogo@hotmail.com)

For prerelease builds, see [this repo](https://gitlab.com/Pr0nogo3/specialbus/-/tree/master).

For more info on HYDRA, including a project introduction and primer, visit the project's [website](http://fraudsclub.com/hydra). You can also follow development progress by consulting our [Trello board](https://trello.com/b/ZuOmqT8i/hydra).

Source code for the plugins used in Project: HYDRA are available in [our tools group](https://gitlab.com/the-no-frauds-club/tools).

# Current caveats:
**Crashes** occur towards the upper end of the bullet and unit arrays. It is not known which cases are causing this crash, and more information needs to be gathered. These crashes are always access violations with exceedingly high memory addresses, so if you receive an access violation crash with a 6-digit memory address, odds are it is not this crash and should be reported.

**AI performance** is not great in certain cases and is being continually adjusted from patch to patch.

# Setup:
If you do not have version 1.16.1 installed, [follow this guide](http://pr0nogo.wikidot.com/sc-play). You should also consult this guide if you encounter any issues with windowed mode, running mods, capturing with recording or streaming software, etc.

Place HYDRA's maps in your maps folder, creating a specific directory for them if you wish.

HYDRA's custom exe and/or the classic installer from FaRTy1billion may alert your antivirus. Starcraft mods use code injectors to launch the game normally before applying the mod's changes, which will always seem suspicious to antivirus software. This mod and FaRTy's installer are both safe for your PC, but feel free to reach out if you have any concerns.

If you run into any other issues or have any other questions after following the guide linked above, message me.

# Multiplayer:
Community games are organized using Radmin VPN, a free and seemingly lightweight network utility.

* Download [Radmin VPN](https://www.radmin-vpn.com/)
* Join the No Frauds Club network with the following credentials:
  * __Network name:__ FRAUDSCLUB
  * __Password:__ toddad69
* Create and join Starcraft games by launching Starcraft, selecting Multiplayer, and selecting Local Area Network (UDP)
* Join the [No Frauds Club discord server](https://discordapp.com/invite/s5SKBmY) to organize community games in real time

If your Radmin VPN is stuck on "waiting for adapter response", it is likely that the software's driver did not install correctly or was somehow corrupted. The only known fix for this is to reinstall the utility.
