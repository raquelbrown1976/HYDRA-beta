This document details features of the melee AI for Project: HYDRA.

Useful links:
Project info - http://pr0nogo.wikidot.com/sc-hydra
Beta builds - https://gitlab.com/the-no-frauds-club/release/HYDRA-beta

Setup for UMS maps:
-Make sure your AI has the correct race set in map settings
-Set disruption field deaths for personalities and dark swarm deaths for build orders (see below)
-To enable debug messages for showing AI personalities and build orders, set start location deaths above 0
-Run <race> expansion custom level in a location placed over the starting town hall
-To check if a player slot is running the melee AI, check for at least 1 scanner sweep death
-Results not guaranteed; the AI have only been tested with melee starting conditions

Modern AI work would not be possible without the contributions of Neiv, iquare, and Veeq7. Special thanks to them all!

An additional special thanks goes out to Keyan and Connor5620, who have tested and documented several of the AI's build orders!

=============
PERSONALITIES
=============
These use disruption field deaths.
Their number corresponds to how many deaths you should set before running the AI.
Without any deaths set, the personality will be chosen at random.

TERRAN
1 - Mech
2 - Raider (bio)
3 - Air
4 - General

PROTOSS
1 - Ranger (Dragoon/Reaver/Air)
2 - Templar (mass Gateway)
3 - Skylord (Air/Reaver)
4 - General

ZERG
1 - Connor (Hydra)
2 - Swarm (mass melee)
3 - Air
4 - General

=============
BUILD  ORDERS
=============
These use dark swarm deaths.
Their number corresponds to how many deaths you should set before running the AI.
Without any deaths set, the build order will be chosen at random.

===TERRAN
General
1 - 2 Factory Mech
2 - 12CC Mix
3 - TenTenTen
4 - BBS (Barracks Barracks Supply)
5 - Mech into Air
6 - Observant (Vessel + mech)

Mech
1 - Tank Timing (Factory first)
2 - FE 5Fact
3 - 2Fact Vult
4 - Walker Power
5 - 4Fact Ghost

Raider
1 - 8Rax					
2 - 3Rax Marine & Medic
3 - Fast Ghost/Nuke
4 - 4Rax Pressure
5 - Uprising (heavy Cyprian usage)
6 - Holy Ackmeds!

Air
1 - 2Port Wraith
2 - Fast Battlecruiser
3 - Wraith Bio
4 - Hotdrop (bio + dropships)

===PROTOSS
General
1 - Forge FE
2 - Nexus FE
3 - 2Gate Robo
4 - Lunatic Legion

Ranger
1 - 2Gate Goon
2 - 1Gate Robo
3 - Forge FE Reaver
4 - 1Gate Expo
5 - Simulant Swarm

Templar
1 - 4Gate
2 - 3Gate Goon
3 - 2Gate DT
4 - Legion
5 - Martyr (fast Grand Library)

Skylord
1 - Forge FE Corsair
2 - Carrier
3 - 2Gate Panoptus
4 - Exalted (fast Arbiter)
5 - Sorrow Fleet (fast Star Sovereign)

===ZERG
General
1 - 2Hatch Hive
2 - 2Hatch Vorv
3 - 3Hatch Hydra Queen
4 - Zoryus Flood

Connor
1 - 3Hatch Hydra
2 - 2Hatch Lurker
3 - 2Hatch Hydraling

Swarm
1 - 3Hatch Flood
2 - 3Hatch Ultra
3 - 9Pool
4 - 3Hatch Hive
5 - Congo Todgod (ling spam)

Air
1 - 3Hatch Mutaling
2 - 2Hatch Guard
3 - 2Hatch Muta